package urlshort

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	urlShort UrlMapping = UrlMapping{
		Path: "/urlshort",
		Url:  "https://github.com/gophercises/urlshort",
	}
	urlShortFinal UrlMapping = UrlMapping{
		Path: "/urlshort-final",
		Url:  "https://github.com/gophercises/urlshort/tree/solution",
	}
)

func TestParseYAML(t *testing.T) {
	yaml := `
    - path: /urlshort
      url: https://github.com/gophercises/urlshort
    - path: /urlshort-final
      url: https://github.com/gophercises/urlshort/tree/solution
    `
	expected := []UrlMapping{urlShort, urlShortFinal}
	actual, err := parseYAML([]byte(yaml))

	if err != nil {
		t.Errorf("Failed with %e", err)
	}
	assert.EqualValues(t, expected, actual, "should be equal")

}

func TestBuildUrlMap(t *testing.T) {
	mappedUrls := []UrlMapping{urlShort, urlShortFinal}

	expectedUrlMap := make(map[string]string)

	expectedUrlMap["/urlshort"] = "https://github.com/gophercises/urlshort"
	expectedUrlMap["/urlshort-final"] = "https://github.com/gophercises/urlshort/tree/solution"

	actualBuiltMap := buildUrlMap(mappedUrls)

	assert.EqualValues(t, expectedUrlMap, actualBuiltMap, "the constructed map should deep equal with expected")

}
