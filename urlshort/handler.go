package urlshort

import (
	"log"
	"net/http"

	"gopkg.in/yaml.v2"
)

type UrlMapping struct {
	Path string
	Url  string
}

// MapHandler will return an http.HandlerFunc (which also
// implements http.Handler) that will attempt to map any
// paths (keys in the map) to their corresponding URL (values
// that each key in the map points to, in string format).
// If the path is not provided in the map, then the fallback
// http.Handler will be called instead.
func MapHandler(pathsToUrls map[string]string, fallback http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if path, ok := pathsToUrls[r.URL.Path]; ok {
			http.Redirect(w, r, path, http.StatusFound)
		}
		fallback.ServeHTTP(w, r)
	}

}

// YAMLHandler will parse the provided YAML and then return
// an http.HandlerFunc (which also implements http.Handler)
// that will attempt to map any paths to their corresponding
// URL. If the path is not provided in the YAML, then the
// fallback http.Handler will be called instead.
//
// YAML is expected to be in the format:
//
//     - path: /some-path
//       url: https://www.some-url.com/demo
//
// The only errors that can be returned all related to having
// invalid YAML data.
//
// See MapHandler to create a similar http.HandlerFunc via
// a mapping of paths to urls.
func YAMLHandler(yml []byte, fallback http.Handler) (http.HandlerFunc, error) {
	parsedYAML, err := parseYAML(yml)

	if err != nil {
		return nil, err
	}

	urlMap := buildUrlMap(parsedYAML)
	return MapHandler(urlMap, fallback), nil
}

func parseYAML(yml []byte) ([]UrlMapping, error) {

	var mappedUrls []UrlMapping

	err := yaml.Unmarshal(yml, &mappedUrls)
	if err != nil {
		return nil, err
	}
	log.Print(mappedUrls)
	return mappedUrls, nil
}

func buildUrlMap(mappedUrls []UrlMapping) map[string]string {
	urlMap := make(map[string]string)
	for _, urlMapping := range mappedUrls {
		urlMap[urlMapping.Path] = urlMapping.Url
	}
	return urlMap
}
